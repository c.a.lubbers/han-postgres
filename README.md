# han-postgres
Start-up a postgres database with docker and an init script:
1. Make a Dockerfile, FROM registry.gitlab.com/c.a.lubbers/han-dockerfile/postgres:13.0-alpine
2. Add a copy step with the init.sql file. See [Initialization scripts
   ](https://hub.docker.com/_/postgres) for where it needs to be placed
3. Make a docker-compose.yml file where you can start-up the postgres database
4. Start up docker-compose.
5. Make a db connection via Intellij to localhost:5432 and insert some data
6. Do a "docker-compose down"
7. When finished do a "docker-compose up"
8. Your data should be gone after the restart
