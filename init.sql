CREATE DATABASE recept_owner;
\connect recept_owner;
GRANT ALL PRIVILEGES ON DATABASE recept_owner TO postgres;

CREATE SCHEMA recept_owner;
GRANT ALL PRIVILEGES ON SCHEMA recept_owner TO postgres;
SET client_encoding TO 'UTF8';

SET search_path = recept_owner, pg_catalog;

CREATE TABLE recept_owner.klant (
    id                  SERIAL PRIMARY KEY,
    naam                VARCHAR(36) NOT NULL
);

CREATE TABLE recept_owner.product (
    id                  SERIAL PRIMARY KEY,
    naam                VARCHAR(36) NOT NULL,
    eenheid             VARCHAR(10) NOT NULL
);

CREATE TABLE recept_owner.recept (
    id                  VARCHAR(36) NOT NULL PRIMARY KEY,
    naam                VARCHAR(36) NOT NULL,
    klant_id            INTEGER     NOT NULL,
    aanmaak_datum       TIMESTAMP   NOT NULL,
    laatst_gewijzigd    TIMESTAMP   NOT NULL DEFAULT LOCALTIMESTAMP
);

CREATE TABLE recept_owner.receptinhoud (
    recept_id           VARCHAR(36) NOT NULL,
    product_id          INTEGER     NOT NULL,
    hoeveelheid         INTEGER     NOT NULL
);

ALTER TABLE recept_owner.recept
  ADD CONSTRAINT fk_klant_id_recept FOREIGN KEY (klant_id) REFERENCES klant (id) ON DELETE CASCADE;


ALTER TABLE recept_owner.receptinhoud
  ADD CONSTRAINT fk_recept_id_receptinhoud FOREIGN KEY (recept_id) REFERENCES recept (id) ON DELETE CASCADE;


ALTER TABLE recept_owner.receptinhoud
  ADD CONSTRAINT fk_product_id_receptinhoud FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE;
